#!/usr/bin/env python3
import math
import sys
import matplotlib.pyplot as plt

e = math.e
pi = math.pi

def fft(x):
    N = len(x)
    if N % 2 != 0 and N != 1:
        print("Len must be power of 2! ({})".format(N))
        sys.exit(-1)
    if len(x) == 1:
        return x
    else:
        Xe = fft(x[::2])
        Xo = fft(x[1::2])
        twiddleFactor = e**(-2j * pi / N)
        #Xe[first half] + Xo[first half] * twiddle, same but with odd rotated 180
        X = [Xe[n] + Xo[n] * twiddleFactor ** n for n in range(len(Xe))]
        X += [Xe[n] - Xo[n] * twiddleFactor ** n for n in range(len(Xe))]
    return X

def freqs(fs, bins):
    size = len(bins)
    freq = []
    shifted = []
    for i in range(-int(size/2),int(size/2)):
        freq += [i/fs * size]
        shifted += [bins[i]]
    return freq, shifted

def sine(fs, f, p=0, a=1):
    return [a*math.sin(2*pi*f*t/fs+p) for t in range(int(fs))]

fs = 1024
fft_size = fs
samps = sine(fs, 40)
bins = fft(samps)
freq, binsm = freqs(fs, bins)
binsm = [abs(x) for x in binsm]

plt.figure(1)
plt.subplot(2,1,1)
plt.plot(samps)
plt.subplot(2,1,2)
plt.plot(freq, binsm)

samps = sine(fs, 10)
bins = fft(samps)
freq, binsm = freqs(fs, bins)
binsm = [abs(x) for x in binsm]
plt.figure(1)
plt.subplot(2,1,1)
plt.plot(samps)
plt.title("Input Signals")
plt.subplot(2,1,2)
plt.plot(freq, binsm)
plt.title("FFT of the two input signals")


plt.show()
