#!/usr/bin/env python3
import math
import matplotlib.pyplot as plt
pi = math.pi
e = math.e

# def idft(size, vals):
def freqs(fs, bins):
    size = len(bins)
    freq = []
    shifted = []
    for i in range(-int(size/2),int(size/2)):
        freq += [i/fs * size]
        shifted += [bins[i]]
    return freq, shifted
    
def dft(size, vals):
    bins = []
    for k in range(int(size)):
        X = 0
        for n in range(int(size)):
            #Can be sped up a lot, some linear algebra and constant calculations can vastly improve this
            X += vals[n] * e ** (-1j * 2 * pi * n * (k/size)) 
        bins += [X]
    return bins


def idft(size, vals):
    bins = []
    for k in range(int(size)):
        X = 0
        for n in range(int(size)):
            #Can be sped up a lot, some linear algebra and constant calculations can vastly improve this
            X += vals[n] * e ** (1j * 2 * pi * n * (k/size))
        bins += [X]
    return bins

def sine(fs, f, p=0, a=1):
    return [a*math.sin(2*pi*f*t/fs+p) for t in range(int(fs))]

fs = 500
dft_size = fs
samps = sine(fs, 40)
bins = dft(dft_size, samps)
freq, binsm = freqs(fs, bins)
binsm = [abs(x) for x in binsm]
plt.figure(1)
plt.subplot(2,1,1)
plt.plot(samps)
plt.subplot(2,1,2)
plt.plot(freq, binsm)

samps = sine(fs, 10)
bins = dft(dft_size, samps)
freq, binsm = freqs(fs, bins)
binsm = [abs(x) for x in binsm]
plt.figure(1)
plt.subplot(2,1,1)
plt.plot(samps)
plt.title("Input Signals")
plt.subplot(2,1,2)
plt.plot(freq, binsm)
plt.title("dft of the two input signals")



invVals = idft(dft_size, bins)
plt.figure(0)
plt.subplot(2,1,1)
freq, bins = freqs(fs, bins)
binsm = [abs(x) for x in bins]
plt.plot(freq, binsm)
plt.title("Input Freq Spectrum")
plt.subplot(2,1,2)
plt.plot(invVals)
plt.title("Idft of Input signal")


plt.show()
